import React from "react";

const search = ({ username, handleChange, handleSearch, experience }) => {
  return (
    <form onSubmit={handleSearch}>
      <legend>Search Data Player</legend>
      <div className="form-group w-25">
        <label htmlFor="search1" className="form-label">
          Username
        </label>
        <input
          type="text"
          className="form-control"
          id="search1"
          name="username"
          onChange={(event) => handleChange(event)}
        ></input>
      </div>

      <div className="form-group w-25">
        <label htmlFor="search2" className="form-label">
          Email
        </label>
        <input
          type="text"
          className="form-control"
          id="search2"
          name="email"
          onChange={(event) => handleChange(event)}
        ></input>
      </div>

      <div className="form-group w-25">
        <label htmlFor="search3" className="form-label">
          Experience
        </label>
        <input
          type="number"
          className="form-control"
          id="search3"
          name="experience"
          onChange={(event) => handleChange(event)}
        ></input>
      </div>

      <div className="form-group w-25">
        <label htmlFor="search4" className="form-label">
          Lvl
        </label>
        <input
          type="number"
          className="form-control"
          id="search4"
          name="lvl"
          onChange={(event) => handleChange(event)}
        ></input>
      </div>

      <br></br>
      <button type="submit" className="btn btn-secondary">
        Search
      </button>
    </form>
  );
};

export default search;
