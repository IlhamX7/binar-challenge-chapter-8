import React, { Component } from "react";
import Tabel from "./Tabel";
import Form from "./Form";
import Search from "./Search";

export default class Player extends Component {
  constructor(props) {
    super(props);
    this.state = {
      players: [],
      id: "",
      username: "",
      email: "",
      password: "",
      experience: "",
      lvl: "",
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    if (this.state.id === "") {
      this.setState({
        players: [
          ...this.state.players,
          {
            id: this.state.players.length + 1,
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            experience: this.state.experience,
            lvl: this.state.lvl,
          },
        ],
      });
    } else {
      const playerYangSelainDipilih = this.state.players
        .filter((player) => player.id !== this.state.id)
        .map((filterPlayer) => {
          return filterPlayer;
        });

      this.setState({
        players: [
          ...playerYangSelainDipilih,
          {
            id: this.state.players.length + 1,
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            experience: this.state.experience,
            lvl: this.state.lvl,
          },
        ],
      });
    }

    this.setState({
      id: "",
      username: "",
      email: "",
      password: "",
      experience: "",
      lvl: "",
    });
  };

  editData = (id) => {
    const playerYangDipilih = this.state.players
      .filter((player) => player.id === id)
      .map((filterPlayer) => {
        return filterPlayer;
      });

    this.setState({
      id: playerYangDipilih[0].id,
      username: playerYangDipilih[0].username,
      email: playerYangDipilih[0].email,
      password: playerYangDipilih[0].password,
      experience: playerYangDipilih[0].experience,
      lvl: playerYangDipilih[0].lvl,
    });
  };

  handleSearch = (event) => {
    event.preventDefault();

    const playerYangDicari = this.state.players
      .filter(
        (player) =>
          player.username === this.state.username ||
          player.email === this.state.email ||
          player.experience === this.state.experience ||
          player.lvl === this.state.lvl
      )
      .map((filterPlayer) => {
        return filterPlayer;
      });

    this.setState({
      players: playerYangDicari,
    });
  };

  render() {
    return (
      <div>
        <div className="container mt-4">
          <Form
            {...this.state}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
          />
          <br></br>
          <Search
            {...this.state}
            handleChange={this.handleChange}
            handleSearch={this.handleSearch}
          />
          <br></br>
          <Tabel players={this.state.players} editData={this.editData} />
        </div>
      </div>
    );
  }
}
