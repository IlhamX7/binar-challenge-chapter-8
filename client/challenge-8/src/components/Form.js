import React from "react";

const form = ({
  username,
  email,
  password,
  experience,
  lvl,
  handleChange,
  handleSubmit,
  id,
}) => {
  return (
    <form onSubmit={handleSubmit}>
      <legend>{id ? "Edit Data Player" : "Tambah Data Player"}</legend>
      <div className="form-group w-25">
        <label htmlFor="username" className="form-label">
          Username
        </label>
        <input
          type="text"
          className="form-control"
          id="username"
          name="username"
          value={username}
          onChange={(event) => handleChange(event)}
        ></input>
      </div>

      <div className="form-group w-25">
        <label htmlFor="email" className="form-label">
          Email
        </label>
        <input
          type="email"
          className="form-control"
          id="email"
          name="email"
          value={email}
          onChange={(event) => handleChange(event)}
        ></input>
      </div>

      <div className="form-group w-25">
        <label htmlFor="password" className="form-label">
          Password
        </label>
        <input
          type="password"
          className="form-control"
          id="password"
          name="password"
          value={password}
          onChange={(event) => handleChange(event)}
        ></input>
      </div>

      <div className="form-group w-25">
        <label htmlFor="experience" className="form-label">
          Experience
        </label>
        <input
          type="number"
          className="form-control"
          id="experience"
          name="experience"
          value={experience}
          onChange={(event) => handleChange(event)}
        ></input>
      </div>

      <div className="form-group w-25">
        <label htmlFor="lvl" className="form-label">
          Lvl
        </label>
        <input
          type="number"
          className="form-control"
          id="lvl"
          name="lvl"
          value={lvl}
          onChange={(event) => handleChange(event)}
        ></input>
      </div>
      <br></br>
      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
};

export default form;
