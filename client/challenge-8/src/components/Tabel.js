import React from "react";

const tabel = ({ players, editData }) => {
  return (
    <table className="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Username</th>
          <th>Email</th>
          <th>Password</th>
          <th>Experience</th>
          <th>Lvl</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {players.map((player, index) => {
          return (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{player.username}</td>
              <td>{player.email}</td>
              <td>{player.password}</td>
              <td>{player.experience}</td>
              <td>{player.lvl}</td>
              <td>
                <button
                  className="btn btn-warning"
                  onClick={() => editData(player.id)}
                >
                  Edit
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default tabel;
